<?php
/*
 * This file is part of mailowl
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\BootgridBundle\Grid\Exception;

/**
 * Class InvalidArgumentException
 * @package Cwd\BootgridBundle\Grid\Exception
 * @author Ludwig Ruderstaler <lr@cwd.at>
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}
