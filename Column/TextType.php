<?php
/*
 * This file is part of cwdBootgridBundle
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\BootgridBundle\Column;

/**
 * Class TextType
 * @package Cwd\BootgridBundle\Column
 * @author Ludwig Ruderstaler <lr@cwd.at>
 */
class TextType extends AbstractColumn
{
}
