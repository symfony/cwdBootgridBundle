<?php
/*
 * This file is part of cwdBootgridBundle
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\BootgridBundle\Column;

use Cwd\BootgridBundle\Grid\Exception\InvalidArgumentException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class ActionType
 * @package Cwd\BootgridBundle\Column
 * @author Ludwig Ruderstaler <lr@cwd.at>
 */
class ActionType extends AbstractColumn
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(array(
            'align' => 'right',
            'actions' => [],
            'actions_params' => [],
            'template' => 'CwdBootgridBundle:Column:actions.html.twig',
        ));

        $resolver->setAllowedTypes('actions', 'array');
        $resolver->setAllowedTypes('actions_params', 'array');
    }

    /**
     * @param mixed            $object
     * @param string           $field
     * @param string           $primary
     * @param PropertyAccessor $accessor
     *
     * @return mixed
     */
    public function getValue($object, $field, $primary, $accessor)
    {
        return $accessor->getValue($object, $primary);
    }
}
