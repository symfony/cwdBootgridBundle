<?php
/*
 * This file is part of CwdBootgridBundle
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\BootgridBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CwdBootgridBundle
 * @package Cwd\BootgridBundle
 * @author Ludwig Ruderstaler <lr@cwd.at>
 */
class CwdBootgridBundle extends Bundle
{
}
